# Automotive Catalytic Converter
The catalyst light-off temperature is a minimum temperature to initiate the catalytic reaction. Before light-off temperature is reached, the catalytic converter is less effective at reducing the pollutants. A typical light-off temperature is between 400 to 600 degrees F. The normal operating temperature is between 750 to 1,600 degrees F. The operating temperature increases with more pollutants in the exhaust.

# Method  
AutoRegressive model with eXogenous input (ARX) time series models are a linear representation of a dynamic system in discrete time. The term exogeneos is related to the fact of being influenced from external parameters. The model relates the current value of a time series to both: i) past values of the same series; and
ii) current and past values of the driving (exogenous) series — that is, of the externally determined series that influences the series of interest. Putting a model into ARX form is the basis for many methods in process dynamics and control analysis. Objective Create an ARX model of the catalytic temperature and coolant temperature based on the vehicle speed. This project makes use of GEKKO python-based package to set the ARX model.

# Data 
Data was collected by Prof. J. D. Green from the Brigham Young university. It includes travel distance, time, fuel rate, air flow, oxygen ratio, and other parameters available from an OBD2 interface.
